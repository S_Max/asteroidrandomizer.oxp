AsteroidRandomizer

By S Max

Moves static asteroids on a really random position.

Available in [Oolite](http://www.oolite.org/) package manager (Ambience).

# License
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

# Version History

## [0.1] - 2016-09-10

* Initial release
