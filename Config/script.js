/* global system log*/

this.name = "AsteroidRandomizer";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Moves static asteroids on a really random position";
this.version = "0.1";

"use strict";

this._DEBUG = false;

this._DIFF_RADIUS = 5000;

this.startUpComplete = function() {
	this._RandomAsteroids();
};

this.shipWillExitWitchspace = function() {
	this._RandomAsteroids();
};

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this._RandomAsteroids = function() {
	var t = system.shipsWithPrimaryRole("asteroid");

	if (t) {
		this._logger("Asteroids in system: " + t.length);
		for (var i = 0; i < t.length; i++) {
			if (this._testShip(t[i])) {
				var v = [];
				for (var j = 0; j < 3; j++) {
					v[j] = Math.random(); // Random number [ 0, 1 )
					v[j] -= 0.5; // Normalize to 0 - [ -0.5, 0.5 )
					v[j] *= this._DIFF_RADIUS * 2; // Set length as _DIFF_RADIUS - [ -_DIFF_RADIUS, _DIFF_RADIUS )
				}
				var pos = t[i].position.add(v); // Adding a random vector to the position
				this._logger("Move: " + t[i].position + " -> " + pos + " diff: " + v);
				t[i].position = pos;
			}
		}
	}
};

this._testShip = function(ship) {
	return ship.isShip && ship.AI == "dumbAI.plist";
};
